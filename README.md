# Langkah Awal 
## 1. build.gradle(app)
### di android tambahkan 
````
    compileOptions {
        sourceCompatibility 1.8
        targetCompatibility 1.8
    }

    kotlinOptions {
        jvmTarget = '1.8'
    }
````
### dan di denpedencies tambahkan
````
// Implementation
    implementation 'com.github.bumptech.glide:glide:4.11.0'
    annotationProcessor 'com.github.bumptech.glide:compiler:4.11.0'
    implementation 'com.google.android.exoplayer:exoplayer:2.10.0'
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.7'

````

## 2. build.grale(StoryApp)
### di allprojects tambahkan mavenCentral()
````
allprojects {
    repositories {
        google()
        jcenter()
        // implementation
        mavenCentral()
    }
}
````
## 3. Masuk ke folder res
### di package value tambahkan attrs.xml
````
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <declare-styleable name="StoriesProgressView">
        <attr name="progressCount" format="integer|reference" />
    </declare-styleable>

</resources>
````
### masih di package value buka color.xml dan tambahkan 
````
<!-- Story Display Progress-->
    <color name="progress_primary">@color/white</color>
    <color name="progress_secondary">@color/gray_scale</color>
    <color name="progress_max_active">@color/white</color>

    <color name="gray_scale">#8AFFFFFF</color>
    <color name="white">#FFFFFF</color>

    <color name="trans">#00000000 </color>
````
### styles.xml
````
<!-- No ActionBar -->
    <style name="AppTheme" parent="Theme.AppCompat.Light.NoActionBar">

        <!-- Customize your theme here. -->
        <item name="colorPrimary">@color/colorPrimary</item>
        <item name="colorPrimaryDark">@color/colorPrimaryDark</item>
        <item name="colorAccent">@color/colorAccent</item>
        <item name="android:windowFullscreen">true</item>
    </style>
````
### Masuk ke package layout tambahkan pausable_progress.xml
````
<?xml version="1.0" encoding="utf-8"?>
<FrameLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content">

    <View
        android:id="@+id/back_progress"
        android:layout_width="match_parent"
        android:layout_height="2dp"
        android:background="@color/progress_secondary" />

    <View
        android:id="@+id/front_progress"
        android:layout_width="match_parent"
        android:layout_height="2dp"
        android:background="@color/progress_primary"
        android:visibility="invisible"
        tools:visibility="visible" />

    <View
        android:id="@+id/max_progress"
        android:layout_width="match_parent"
        android:layout_height="2dp"
        android:background="#fff"
        android:visibility="gone" />

</FrameLayout>
````
### Buka package drawable tambahkan story_top_shadow.xml
````
<?xml version="1.0" encoding="UTF-8"?>
<shape xmlns:android="http://schemas.android.com/apk/res/android"
    android:shape="rectangle">

    <gradient
        android:angle="90"
        android:endColor="#4f000000"
        android:startColor="#00ffffff"
        android:type="linear" />

</shape>
````
## Masuk folder java, tambahkan package app, customeview, data, screen, utils.
### 1. Masuk ke package data tambahkan Story.kt dan StoryUser.kt
#### Story.kt
````
/***
 * Parcelize is an annotation provided by Kotlin Android Extensions that will automatically generate
 * theserialization implementation for your custom Parcelable type at compile time
 */
// import plugin Parcelize
@Parcelize
// create data class Story and extand Parcelabe
data class Story(
    // define parameter url with type string
    val url: String
)
    : Parcelable {

    // define function isVideo for showing only video .mp4
    fun isVideo() = url.contains(".mp4")
}
````
#### StoryUser.kt
````
// import plugin Parcelize
@Parcelize
// create data class User and extand Parcelable
data class StoryUser(
    // define parameter username with type String
    val username: String,
    // define parameter profilePicUrl with type String
    val profilePicUrl: String,
    // define parameter stories with type ArrayList<Story>
    val stories: ArrayList<Story>
)
    : Parcelable
````
### 2. Masuk ke package utils tambahkan CubeOutTransformer.kt, OnSwipeTouchListener.kt, PausableScaleAnimation, StoryGenerator, ViewExt.kt
#### Tambahkan CubeOutTransformer.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/utils/CubeOutTransformer.kt)
#### Tambahkan OnSwipeTouchListener.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/utils/OnSwipeTouchListener.kt)
#### Tambahkan PausableScaleAnimation.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/utils/PausableScaleAnimation.kt)
#### Tambahkan object StoryGenerator.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/utils/StoryGenerator.kt)
#### Tambahkan ViewExt.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/utils/ViewExt.kt)
### 3. Masuk folder app tambahkan StoryApp.kt
#### Tambahkan StoryApp.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/app/StoryApp.kt)
### 4. Masuk folder customeview tambahkan FixedViewPager.kt, PausableProgressBar.kt, StoriesProgressView.kt, StoryPagerAdapter
#### Tambahkan FixedViewPager.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/customview/FixedViewPager.kt)
#### Tambahkan PausableProgressBar.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/customview/PausableProgressBar.kt)
#### Tambahkan StoriesProgressView.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/customview/StoriesProgressView.kt)
#### Tambahkan StoryPagerAdapter.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/customview/StoryPagerAdapter.kt)
### 5. Masuk folder screen tambahkan PageViewOperator.kt, PageChangeListener, StoryDisplayFragment dan layout, HomeFragment dan layout, MainFragment.kt dan layout, dan isian MainActivity.kt
#### Tambahkan interface PageViewOperator.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/screen/PageViewOperator.kt)
#### Tambahkan PageChangeListener.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/screen/PageChangeListener.kt)
#### Tambahkan StoryDisplayFragment dan layoutnya
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/screen/StoryDisplayFragment.kt)
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/res/layout/fragment_story_display.xml)
#### Tambahkan HomeFragment.kt dan layoutnya
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/screen/HomeFragment.kt)
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/res/layout/fragment_home.xml)
#### Tambahkan MainFragment.kt dan layoutnya
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/screen/MainFragment.kt)
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/res/layout/fragment_main.xml)
#### isi tambahan MainActivity.kt
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/java/com/example/cobastoryfragment/screen/MainActivity.kt)
(https://gitlab.com/rizkysubagja2020/fragmentstory1/-/blob/master/app/src/main/res/layout/activity_main.xml)

# UPDATE
## fragment_story_display.xml
````
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <com.google.android.exoplayer2.ui.PlayerView
        android:id="@+id/storyDisplayVideo"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:visibility="gone"
        app:surface_type="texture_view"/>

    <androidx.appcompat.widget.AppCompatImageView
        android:id="@+id/storyDisplayImage"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:scaleType="centerCrop"/>

    <ProgressBar
        android:id="@+id/storyDisplayVideoProgress"
        style="?android:attr/progressBarStyleInverse"
        android:layout_width="40dp"
        android:layout_height="40dp"
        android:layout_margin="8dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent"/>

    <Button
        android:id="@+id/clickHere"
        android:layout_marginBottom="20dp"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/click_here"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="@+id/storyDisplayImage"
        app:layout_constraintStart_toStartOf="@+id/storyDisplayImage" />

    <ImageButton
        android:id="@+id/close"
        android:layout_width="40dp"
        android:layout_height="40dp"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        android:layout_marginTop="20dp"
        android:layout_marginRight="50dp"
        android:layout_marginEnd="30dp"

        android:background="@color/trans"
        android:src="@drawable/ic_action_cancel"/>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:id="@+id/storyOverlay"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        app:layout_constraintTop_toTopOf="parent">

        <View
            android:layout_width="match_parent"
            android:layout_height="60dp"
            android:background="@drawable/story_top_shadow"
            app:layout_constraintBottom_toTopOf="parent"/>

        <com.example.cobastoryfragment.customview.StoriesProgressView
            android:id="@+id/storiesProgressView"
            android:layout_width="match_parent"
            android:layout_height="3dp"
            android:layout_gravity="top"
            android:layout_marginTop="8dp"
            android:paddingLeft="8dp"
            android:paddingRight="8dp"
            app:layout_constraintTop_toTopOf="parent"/>

        <LinearLayout
            android:id="@+id/storyDisplayProfile"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:gravity="center_vertical"
            android:orientation="horizontal"
            android:padding="4dp"
            app:layout_constraintTop_toBottomOf="@+id/storiesProgressView">

            <androidx.appcompat.widget.AppCompatImageView
                android:id="@+id/storyDisplayProfilePicture"
                android:layout_width="34dp"
                android:layout_height="34dp"
                android:layout_margin="8dp"/>

            <LinearLayout
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:orientation="vertical">

                <TextView
                    android:id="@+id/storyDisplayNick"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:textColor="#ffffff"
                    android:textSize="13sp"
                    android:textStyle="bold"
                    tools:text="username"/>

            </LinearLayout>

        </LinearLayout>

    </androidx.constraintlayout.widget.ConstraintLayout>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:paddingTop="70dp"
        android:orientation="horizontal">

        <View
            android:id="@+id/previous"
            android:layout_width="0dp"
            android:layout_height="match_parent"
            android:layout_weight="3"/>
        <View
            android:id="@+id/paused"
            android:layout_width="0dp"
            android:layout_height="match_parent"
            android:layout_weight="2"/>
        <View
            android:id="@+id/next"
            android:layout_width="0dp"
            android:layout_height="match_parent"
            android:layout_weight="3"/>
    </LinearLayout>

</androidx.constraintlayout.widget.ConstraintLayout>

````

## Tambahkan function berikut didalam StoryDisplayFragment
````
private fun closeFragment(v: View?) {
        val intent = Intent(requireActivity().application, MainActivity::class.java)
        startActivity(intent)
    }
````
## Kemudian set Button dan function tersebut(Masih di StoryDisplayFragment di dalam OnViewCreated)
````
override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        storyDisplayVideo.useController = false
        // ----- Tambahkan -----
        val closeButton = requireView().findViewById(R.id.close) as ImageButton
        val clickMe = requireView().findViewById(R.id.clickHere) as Button

        //
        closeButton.setOnClickListener {
            closeFragment(view)
        }
        clickMe.setOnClickListener {
            closeFragment(view)
        }
        // ----- Selesai -----
        Log.d("StoryDisplayFragment", "ON VIEW CREATE")

        // called updateStory
        updateStory()
        // called setUpUi
        setUpUi()

    }
````
