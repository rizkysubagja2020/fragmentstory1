package com.example.cobastoryfragment.utils

import com.example.cobastoryfragment.data.Story
import com.example.cobastoryfragment.data.StoryUser
import kotlin.random.Random

object StoryGenerator {
    // declare function generateStories with type ArrayList<StoryUser>
    fun generateStories(): ArrayList<StoryUser> {
        // define variable storyUrls with value ArrayList<String>
        val storyUrls = ArrayList<String> ()
        // add link url into storyUrls
        storyUrls.add("https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4")
        storyUrls.add("https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_640_3MG.mp4")
        storyUrls.add("https://images.pexels.com/photos/1433052/pexels-photo-1433052.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/1366630/pexels-photo-1366630.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/1067333/pexels-photo-1067333.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/1122868/pexels-photo-1122868.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/1837603/pexels-photo-1837603.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/1612461/pexels-photo-1612461.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/1591382/pexels-photo-1591382.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/53757/pexels-photo-53757.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500")
        storyUrls.add("https://images.pexels.com/photos/134020/pexels-photo-134020.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/1367067/pexels-photo-1367067.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/1420226/pexels-photo-1420226.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500")
        storyUrls.add("https://images.pexels.com/photos/2217365/pexels-photo-2217365.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/2260800/pexels-photo-2260800.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/1719344/pexels-photo-1719344.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/364096/pexels-photo-364096.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/3849168/pexels-photo-3849168.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/2953901/pexels-photo-2953901.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/3538558/pexels-photo-3538558.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
        storyUrls.add("https://images.pexels.com/photos/2458400/pexels-photo-2458400.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")

        // define variable userProfileUrls with value ArrayList<String>
        val userProfileUrls = ArrayList<String>()
        // add link random picture into userProfileUrls
        userProfileUrls.add("https://randomuser.me/api/portraits/women/1.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/men/1.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/women/2.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/men/2.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/women/3.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/men/3.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/women/4.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/men/4.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/women/5.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/men/5.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/women/6.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/men/6.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/women/7.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/men/7.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/women/8.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/men/8.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/women/9.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/men/9.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/women/10.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/men/10.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/women/11.jpg")
        userProfileUrls.add("https://randomuser.me/api/portraits/men/11.jpg")

        // define storyUserList with value ArrayList<StoryUser>()
        val storyUserList = ArrayList<StoryUser>()

        // do something
        for (index in 1..10) { // total user
            // define variable stories with value ArrayList<Story>()
            val stories = ArrayList<Story>()
            // define variable storySize with value Random (from 1 until 5)
            val storySize = Random.nextInt(1, 5)
            // do something, input data into variable stories
            for (jpg in 0 until storySize) {
                stories.add(
                    Story(
                        // random image story
                        storyUrls[Random.nextInt(storyUrls.size)]
                    )
                )
            }
            // input data into storyUserList
            storyUserList.add(
                StoryUser(
                    // username
                    "Nama Panjang$index",
                    // random picture user
                    userProfileUrls[Random.nextInt(userProfileUrls.size)],
                    stories
                )
            )
        }
        // return
        return storyUserList
    }
}