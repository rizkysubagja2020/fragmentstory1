package com.example.cobastoryfragment.utils

import android.view.View

// define function hide for hiding view
fun View.hide() {
    visibility = View.GONE
}

// define function show for showing view
fun View.show() {
    visibility = View.VISIBLE
}