package com.example.cobastoryfragment.screen

import android.animation.Animator
import android.animation.ValueAnimator
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.SparseIntArray
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import com.bumptech.glide.Glide
import com.example.cobastoryfragment.R
import com.example.cobastoryfragment.app.StoryApp
import com.example.cobastoryfragment.customview.StoryPagerAdapter
import com.example.cobastoryfragment.data.StoryUser
import com.example.cobastoryfragment.utils.CubeOutTransformer
import com.example.cobastoryfragment.utils.StoryGenerator
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DataSpec
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheUtil
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class MainFragment : Fragment() {

    // define property pageAdapater with typedata StoryPagerAdapter
    private lateinit var pagerAdapter: StoryPagerAdapter
    // define property currentPage with type Int and value 0
    private var currentPage: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpPager()
    }

    // Create function setUpPager
    private fun setUpPager() {
        //
        val storyUserList = StoryGenerator.generateStories()
        // called preloadStories
        preLoadStories(storyUserList)

        // declare adapater
        pagerAdapter = StoryPagerAdapter(
            childFragmentManager,
            storyUserList
        )
        // declare adapter viewpager
        viewPager.adapter = pagerAdapter
        viewPager.currentItem = currentPage
        // page transformer
        viewPager.setPageTransformer(
            true,

            // called CubeOutTransformer
            CubeOutTransformer()
        )
        viewPager.addOnPageChangeListener(object : PageChangeListener() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                currentPage = position
            }

            override fun onPageScrollCanceled() {
                currentFragment()?.resumeCurrentStory()
            }
        })
    }

    // declare function preLoadStories with parameter storyUserList: ArrayList<StoryUser>
    private fun preLoadStories(storyUserList: ArrayList<StoryUser>) {
        // define variable imageList with value mutableList<String>
        val imageList = mutableListOf<String>()
        // define variable videoList with value mutableList<String>
        val videoList = mutableListOf<String>()

        //
        storyUserList.forEach { storyUser ->
            storyUser.stories.forEach { story ->
                if (story.isVideo()) {
                    videoList.add(story.url)
                } else {
                    imageList.add(story.url)
                }
            }
        }

        // called function preLoadVideos with variable videoList
        preLoadVideos(videoList)
        // called function preLoadImages with variable imageList
        preLoadImages(imageList)
    }

    // declare function preLoadVideos
    private fun preLoadVideos(videoList: MutableList<String>) {
        videoList.map { data ->
            GlobalScope.async {
                val dataUri = Uri.parse(data)
                val dataSpec = DataSpec(dataUri, 0, 500 * 1024, null)
                val dataSource: DataSource =
                    DefaultDataSourceFactory(
                        requireContext(),
                        Util.getUserAgent(requireContext(), getString(R.string.app_name))
                    ).createDataSource()

                val listener =
                    CacheUtil.ProgressListener { requestLength: Long, bytesCached: Long, _: Long ->
                        val downloadPercentage = (bytesCached * 100.0
                                / requestLength)
                        Log.d("preLoadVideos", "downloadPercentage: $downloadPercentage")
                    }

                try {
                    CacheUtil.cache(
                        dataSpec,
                        StoryApp.simpleCache,
                        CacheUtil.DEFAULT_CACHE_KEY_FACTORY,
                        dataSource,
                        listener,
                        null
                    )
                } catch (exception: Exception) {
                    exception.printStackTrace()
                }
            }
        }
    }

    // declare function pre load image
    private fun preLoadImages(imageList: MutableList<String>) {
        imageList.forEach { imageStory ->
            // use Glide
            Glide.with(this).load(imageStory).preload()
        }
    }

    // declare function currentFragment extends StoryDisplayFragment
    private fun currentFragment(): StoryDisplayFragment? {
        // return pagerAdapter to another fragment
        return pagerAdapter.findFragmentByPosition(viewPager, currentPage) as StoryDisplayFragment
    }



    // create companion object progressState for map integers to integers
    companion object {
        /**
         *  SparseIntArrays map integers to integers. Unlike a normal array of integers, there can be
         *  gaps in the indices. It is intended to be more memory efficient than using a HashMap to map
         *  Integers to Integers, both because it avoids auto-boxing keys and values and its data
         *  structure doesn't rely on an extra entry object for each mapping
         * */
        // define property progressState with type SparseIntArray
        val progressState = SparseIntArray()
    }
}