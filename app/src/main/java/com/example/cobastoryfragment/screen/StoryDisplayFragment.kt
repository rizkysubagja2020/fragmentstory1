package com.example.cobastoryfragment.screen

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.cobastoryfragment.R
import com.example.cobastoryfragment.app.StoryApp
import com.example.cobastoryfragment.customview.StoriesProgressView
import com.example.cobastoryfragment.data.Story
import com.example.cobastoryfragment.data.StoryUser
import com.example.cobastoryfragment.utils.OnSwipeTouchListener
import com.example.cobastoryfragment.utils.hide
import com.example.cobastoryfragment.utils.show
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.fragment_story_display.*
import java.util.ArrayList

class StoryDisplayFragment : Fragment(), StoriesProgressView.StoriesListener {

    // lazi itu untuk late init untuk val
    private val position: Int by
    lazy { arguments?.getInt(EXTRA_POSITION) ?: 0 }

    private val storyUser: StoryUser by
    lazy {
        (arguments?.getParcelable<StoryUser>(
            EXTRA_STORY_USER
        ) as StoryUser)
    }

    private val stories: ArrayList<Story> by
    lazy { storyUser.stories }

    // define property simpleExoPlayer with type SimpleExoPlayer and value null
    private var simpleExoPlayer: SimpleExoPlayer? = null
    // define property mediaDataSourceFactory with type interface DataSource.Factory
    private lateinit var mediaDataSourceFactory: DataSource.Factory
    // define property pageViewOperator with type interface PageViewOperator and value null
    private var pageViewOperator: PageViewOperator? = null
    // define property counter with value 0
    private var counter = 0
    // define property pressTime with value 0L
    private var pressTime = 0L
    // define property limit with value 500L
    private var limit = 500L
    // define property onREsumeCalled with value false
    private var onResumeCalled = false
    // define property onVideoPrepared with value false
    private var onVideoPrepared = false

    /**
     * Override methode onCreateView, onViewCreated, onAttach, onStart, onResume, onPause, onDestroyView
     * */
    // funtion onCreateView creates and returns the view hierarchy associated with the fragment.
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_story_display, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // called updateStory
        updateStory()
        // called setUpUi
        setUpUi()
    }

    // function onAttach called when a Fragment is first attached to a host Activity. After this method, onCreate()is called
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.pageViewOperator = context as PageViewOperator
    }

    // function onStart makes the fragment visible to the user (based on its containing activity being started).
    override fun onStart() {
        super.onStart()
        counter = restorePosition()
    }

    // function OnResume makes the fragment begin interacting with the user (based on its containing activity being resumed).
    override fun onResume() {
        super.onResume()
        // define property onResumeCalled to true
        onResumeCalled = true

        // do something
        if (stories[counter].isVideo() && !onVideoPrepared) {
            simpleExoPlayer?.playWhenReady = false
            return
        }

        //
        simpleExoPlayer?.seekTo(5)
        simpleExoPlayer?.playWhenReady = true
        if (counter == 0) {
            storiesProgressView?.startsStories()
        } else {
            // restart animation
            counter = MainFragment.progressState.get(arguments?.getInt(EXTRA_POSITION) ?: 0)
            storiesProgressView?.startStories(counter)
        }
    }

    // function onPause makes fragment is no longer interacting with the user either because its fragment is being paused
    override fun onPause() {
        super.onPause()
        simpleExoPlayer?.playWhenReady = false
        storiesProgressView?.abandon()
    }

    // function onDestroyView to clean up resources associated with its View
    override fun onDestroyView() {
        super.onDestroyView()

        simpleExoPlayer?.release()
    }
    /**
     * Impelement function onNext, onPrev, onComplete from StoriesProgressView.StoriesListener
     */
    //  function onNext to the next StoriesProgressView
    override fun onNext() {
        // do something
        if (stories.size <= counter + 1) {
            return
        }
        ++counter

        // save position
        savePosition(counter)
        // update story
        updateStory()
    }

    // function onPrev to the previous StoriesProgressView
    override fun onPrev() {
        // do something
        if (counter - 1 < 0) return
        --counter

        // save position
        savePosition(counter)
        // update story
        updateStory()
    }

    // function onComplete
    override fun onComplete() {
        simpleExoPlayer?.release()
        pageViewOperator?.nextPageView()
    }

    // create function update story
    private fun updateStory() {
        simpleExoPlayer?.stop()
        if (stories[counter].isVideo()) {
            // called storyDisplayVideo with function show
            storyDisplayVideo.show()
            // called sotryDisplayImage with function hide
            storyDisplayImage.hide()
            // called storyDisplayProgress with function show
            storyDisplayVideoProgress.show()

            // return initialize
            initializePlayer()
        } else {
            // called storyDisplayVideo with function hide
            storyDisplayVideo.hide()
            // called storyDisplayImage with function show
            storyDisplayImage.show()
            // called storyDisplayProgress with function hide
            storyDisplayVideoProgress.hide()

            // return Image with Glide
            Glide.with(this).load(stories[counter].url).into(storyDisplayImage)
        }
    }

    // create function initialize Player
    private fun initializePlayer() {
        if (simpleExoPlayer == null) {
            simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(requireContext())
        } else {
            simpleExoPlayer?.release()
            simpleExoPlayer = null
            simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(requireContext())
        }

        mediaDataSourceFactory = CacheDataSourceFactory(
            StoryApp.simpleCache,
            DefaultHttpDataSourceFactory(
                Util.getUserAgent(
                    context,
                    Util.getUserAgent(requireContext(), getString(R.string.app_name))
                )
            )
        )
        // Define variable media source
        val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory).createMediaSource(
            Uri.parse(stories[counter].url)
        )
        simpleExoPlayer?.prepare(mediaSource, false, false)
        // do something
        if (onResumeCalled) {
            simpleExoPlayer?.playWhenReady = true
        }

        storyDisplayVideo.setShutterBackgroundColor(Color.BLACK)
        storyDisplayVideo.player = simpleExoPlayer

        /**
         * EventListener is Listener of changes in player state. All methods have no-op default
         * implementations to allow selective overrides.
         */
        // add EventLister and declare funtion OnPlayerError and onLoadingChanged
        simpleExoPlayer?.addListener(object : Player.EventListener {
            // function onPlayerError loads in case of an error
            override fun onPlayerError(error: ExoPlaybackException?) {
                super.onPlayerError(error)
                storyDisplayVideoProgress.hide()
                if (counter == stories.size.minus(1)) {
                    pageViewOperator?.nextPageView()
                } else {
                    storiesProgressView?.skip()
                }
            }

            // function onLoadingChanged for when the video or image loading, the time will be pause
            override fun onLoadingChanged(isLoading: Boolean) {
                super.onLoadingChanged(isLoading)
                if (isLoading) {
                    storyDisplayVideoProgress.show()
                    pressTime = System.currentTimeMillis()

                    //called pause
                    pauseCurrentStory()
                } else {
                    storyDisplayVideoProgress.hide()
                    storiesProgressView?.getProgressWithIndex(counter)
                        ?.setDuration(simpleExoPlayer?.duration ?: 8000L)
                    onVideoPrepared = true

                    // called resume
                    resumeCurrentStory()
                }
            }
        })
    }

    // declare function setUpUi for UI
    private fun setUpUi() {
        //define variable touchListener with object OnSwipeTouchListener
        val touchListener = object : OnSwipeTouchListener(activity!!) {
            // Override function OnSwipeTop, onSwipeBottom, onClick, onLongClick, onTouchView
            override fun onSwipeTop() {
                Toast.makeText(activity, "onSwipeTop", Toast.LENGTH_LONG).show()
            }

            override fun onSwipeBottom() {
                Toast.makeText(activity, "onSwipeBottom", Toast.LENGTH_LONG).show()
            }

            // declare funtion onClick to view
            override fun onClick(view: View) {
                when (view) {
                    // next is the id of View in LinearLayout (fragment_story_display.xml)
                    next -> {
                        // do something
                        if (counter == stories.size - 1) {
                            // into next page
                            pageViewOperator?.nextPageView()
                        } else {
                            // into next item view
                            storiesProgressView?.skip()
                        }
                    }
                    // previous is the id of View in LinearLayout (fragment_story_display.xml)
                    previous -> {
                        // do something
                        if (counter == 0) {
                            // into previos page
                            pageViewOperator?.backPageView()
                        } else {
                            // into previos item view
                            storiesProgressView?.reverse()
                        }
                    }
                }
            }

            // declare funtion onLongClick for view
            override fun onLongClick() {
                // called hideStoryOverlay
                hideStoryOverlay()
            }

            // declare function onTouchView
            override fun onTouchView(view: View, event: MotionEvent): Boolean {
                /**
                 * Motion events describe movements in terms of an action code and a set of axis values.
                 * The action code specifies the state change that occurred such as a pointer going down or up.
                 * The axis values describe the position and other movement properties.
                 * */
                super.onTouchView(view, event)
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        pressTime = System.currentTimeMillis()
                        // called pauseCurrentStory
                        pauseCurrentStory()
                        return false
                    }
                    MotionEvent.ACTION_UP -> {
                        showStoryOverlay()
                        // called resumeCurrentStory
                        resumeCurrentStory()
                        return limit < System.currentTimeMillis() - pressTime
                    }
                }
                return false
            }
        }

        // set listener into previos
        previous.setOnTouchListener(touchListener)
        // set listener into next
        next.setOnTouchListener(touchListener)

        storiesProgressView?.setStoriesCountDebug(
            stories.size, position = arguments?.getInt(EXTRA_POSITION) ?: -1
        )

        // set duration into storiesProgressView
        storiesProgressView?.setAllStoryDuration(4000L)
        // set listener into storiesProgressView
        storiesProgressView?.setStoriesListener(this)

        // return image profile user and name user
        Glide.with(this).load(storyUser.profilePicUrl).circleCrop().into(storyDisplayProfilePicture)
        storyDisplayNick.text = storyUser.username
    }

    // create function showStoryOverlay
    private fun showStoryOverlay() {
        if (storyOverlay == null || storyOverlay.alpha != 0F) return

        storyOverlay.animate()
            .setDuration(100)
            .alpha(1F)
            .start()
    }

    // create function hideStoryOverlay
    private fun hideStoryOverlay() {
        // do something (storyOverlay is the id of ConstraintLayout in ConstrainLayout)
        if (storyOverlay == null || storyOverlay.alpha != 1F) return

        storyOverlay.animate()
            .setDuration(200)
            .alpha(0F)
            .start()
    }

    // declare function savePosition for saving position
    private fun savePosition(pos: Int) {
        MainFragment.progressState.put(position, pos)
    }

    // declare function restorePosition for restored position
    private fun restorePosition(): Int {
        return MainFragment.progressState.get(position)
    }

    // declare function pauseCurrentStory for pause current story
    fun pauseCurrentStory () {
        // do something
        simpleExoPlayer?.playWhenReady = false
        // pause storieProgressView
        storiesProgressView?.pause()
    }

    // declare function resumeCurrentStory for resume current story
    fun resumeCurrentStory() {
        // do something
        if (onResumeCalled) {
            simpleExoPlayer?.playWhenReady = true

            // called showStoryOverlay
            showStoryOverlay()
            // resume storiesProgressView
            storiesProgressView?.resume()
        }
    }

    // create companion object
    companion object {
        private const val EXTRA_POSITION = "EXTRA_POSITION"
        private const val EXTRA_STORY_USER = "EXTRA_STORY_USER"

        // declare function newInstance extand StoryDisplayFragment
        fun newInstance(position: Int, story: StoryUser): StoryDisplayFragment {
            /** apply, Calls the specified function block with this value as its receiver and returns this value */
            return StoryDisplayFragment().apply {
                arguments = Bundle().apply {
                    putInt(EXTRA_POSITION, position)
                    putParcelable(EXTRA_STORY_USER, story)
                }
            }
        }
    }
}