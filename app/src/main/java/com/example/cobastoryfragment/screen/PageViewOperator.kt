package com.example.cobastoryfragment.screen

interface PageViewOperator {

    // define function backPageView
    fun backPageView()
    // define function nextPageView
    fun nextPageView()
}