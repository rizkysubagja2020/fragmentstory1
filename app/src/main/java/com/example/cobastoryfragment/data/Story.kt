package com.example.cobastoryfragment.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/***
 * Parcelize is an annotation provided by Kotlin Android Extensions that will automatically generate
 * theserialization implementation for your custom Parcelable type at compile time
 */
// import plugin Parcelize
@Parcelize
// create data class Story and extand Parcelabe
data class Story(
    // define parameter url with type string
    val url: String
)
    : Parcelable {

    // define function isVideo for showing only video .mp4
    fun isVideo() = url.contains(".mp4")
}