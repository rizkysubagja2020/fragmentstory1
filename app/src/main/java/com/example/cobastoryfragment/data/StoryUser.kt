package com.example.cobastoryfragment.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

// import plugin Parcelize
@Parcelize
// create data class User and extand Parcelable
data class StoryUser(
    // define parameter username with type String
    val username: String,
    // define parameter profilePicUrl with type String
    val profilePicUrl: String,
    // define parameter stories with type ArrayList<Story>
    val stories: ArrayList<Story>
)
    : Parcelable