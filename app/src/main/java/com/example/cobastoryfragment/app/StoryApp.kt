package com.example.cobastoryfragment.app

import android.app.Application
import com.google.android.exoplayer2.database.DatabaseProvider
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache

class StoryApp : Application() {
    override fun onCreate() {
        super.onCreate()
        /** Evicts least recently used cache files first. */
        // declare variable leastRecent with value LeastRecent(90 * 1024 * 1024)
        val leastRecentlyUsedCacheEvictor = LeastRecentlyUsedCacheEvictor(90 * 1024 * 1024)

        /** An {@link SQLiteOpenHelper} that provides instances of a standalone ExoPlayer database. */
        // declare variabel databaseProv with type DatabasePorv and value ExoDatabaseProvider(this)
        val databaseProvider: DatabaseProvider = ExoDatabaseProvider(this)

        // do something
        if (simpleCache == null) {
            simpleCache = SimpleCache(cacheDir, leastRecentlyUsedCacheEvictor, databaseProvider)
        }

    }

    // create companion object
    companion object {
        /**
         * SimpleCache is a implementation that maintains an in-memory representation.
         */
        // declare variable simpleCache with type SimpleCache and value null
        var simpleCache: SimpleCache? = null
    }
}