package com.example.cobastoryfragment.customview

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.cobastoryfragment.data.StoryUser
import com.example.cobastoryfragment.screen.StoryDisplayFragment

class StoryPagerAdapter constructor(fragmentManager: FragmentManager, private val storyList: ArrayList<StoryUser>)
    : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    /**
     * Implement methods getItem and getCount
     */
    // get item from StoryDisplayFragment with newInstance
    override fun getItem(position: Int): Fragment =
        StoryDisplayFragment.newInstance(position, storyList[position])

    // get count size from storyList
    override fun getCount(): Int {
        return storyList.size
    }

    // declare function find fragment by position extand Fragment
    fun findFragmentByPosition(viewPager: ViewPager, position: Int): Fragment? {
        try {
            val f = instantiateItem(viewPager, position)
            return f as? Fragment
        } finally {
            finishUpdate(viewPager)
        }
    }
}