package com.example.cobastoryfragment.customview

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

/** JvmOverload Instructs the Kotlin compiler to generate overloads for this function that
 *  substitute default parameter values.*/
// create custom viewPager
class FixedViewPager @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) :
    ViewPager(context, attrs) {

    // override function onInterceptTouchEvent, prevent NPE if fake dragging and touching ViewPager
    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        // Do something
        return if (isFakeDragging) {
            false
        } else try {
            super.onInterceptTouchEvent(ev)
        } catch (exception: IllegalArgumentException) {
            false
        }
    }
}