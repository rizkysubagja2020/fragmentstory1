package com.example.cobastoryfragment.customview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.example.cobastoryfragment.R
import java.util.ArrayList

class StoriesProgressView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet,
    defStyle: Int = 0

) : LinearLayout(context, attrs, defStyle) {

    private val progressBars: MutableList<PausableProgressBar> = ArrayList()
    private var storiesListener: StoriesListener? = null
    private var storiesCount = 0
    private var current = -1
    private var isSkipStart = false
    private var isReverseStart = false
    private var position = -1
    private var isComplete = false

    //
    init {
        // make a Horizontal
        orientation = HORIZONTAL
        // define variable typeArray for StoriesProgressView
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.StoriesProgressView)
        storiesCount = typedArray.getInt(R.styleable.StoriesProgressView_progressCount, 0)
        typedArray.recycle()

        // called function bindViews
        bindViews()
    }

    // declare function bindViews
    private fun bindViews() {
        // clear the progressBar
        progressBars.clear()
        // remove all views
        removeAllViews()
        // do something
        for (index in 0 until storiesCount) {
            val p = createProgressBar()
            p.tag = "p($position) c($index)" // debug
            // add progress bar
            progressBars.add(p)
            // add view
            addView(p)
            // do something
            if (index + 1 < storiesCount) {
                // add item view
                addView(createSpace())
            }
        }
    }

    // declare function createProgressBar extand PausableProgressBar
    private fun createProgressBar(): PausableProgressBar {
        /** "apply" Calls the specified function block with this value as its receiver and returns this value */
        return PausableProgressBar(context).apply {
            // set layoutParams with PROGRESS_BAR_LAYOUT_PARAM
            layoutParams = PROGRESS_BAR_LAYOUT_PARAM
        }
    }

    // declare function createSpace extand View
    private fun createSpace(): View {
        /** apply, Calls the specified function block with this value as its receiver and returns this value */
        return View(context).apply {
            // set layoutParams with SPACE_LAYOUT_PARAM
            layoutParams = SPACE_LAYOUT_PARAM
        }
    }

    // declare funtion callBack extand PausableProgressBar.Callback
    private fun callback(index: Int): PausableProgressBar.Callback {
        return object : PausableProgressBar.Callback {
            override fun onStartProgress() {
                current = index
            }

            //
            override fun onFinishProgress() {
                if (isReverseStart) {
                    if (storiesListener != null) storiesListener!!.onPrev()
                    if (0 <= current - 1) {
                        val p = progressBars[current - 1]
                        p.setMinWithoutCallback()
                        progressBars[--current].startProgress()
                    } else {
                        progressBars[current].startProgress()
                    }
                    isReverseStart = false
                    return
                }
                val next = current + 1
                if (next <= progressBars.size - 1) {
                    if (storiesListener != null) storiesListener!!.onNext()
                    progressBars[next].startProgress()
                    ++current
                } else {
                    isComplete = true
                    if (storiesListener != null) storiesListener!!.onComplete()
                }
                isSkipStart = false
            }
        }
    }

    // declare function setStoriesCountDebug for set count stories
    fun setStoriesCountDebug(storiesCount: Int, position: Int) {
        this.storiesCount = storiesCount
        this.position = position

        // called function bindViews
        bindViews()
    }

    // declare function setStoriesListener for  set sotries listener
    fun setStoriesListener(storiesListener: StoriesListener?) {
        this.storiesListener = storiesListener
    }

    // declare function skip
    fun skip() {
        if (isSkipStart || isReverseStart) return
        if (isComplete) return
        if (current < 0) return
        val p = progressBars[current]
        isSkipStart = true
        p.setMax()
    }

    // declare function reverse
    fun reverse() {
        if (isSkipStart || isReverseStart) return
        if (isComplete) return
        if (current < 0) return
        val p = progressBars[current]
        isReverseStart = true
        p.setMin()
    }

    // declare function setAllStoryDuration
    fun setAllStoryDuration(duration: Long) {
        for (i in progressBars.indices) {
            progressBars[i].setDuration(duration)
            progressBars[i].setCallback(callback(i))
        }
    }

    // declare function startsStories
    fun startsStories() {
        // do something
        if (progressBars.size > 0) {
            progressBars[0].startProgress()
        }
    }

    // declare function startStories
    fun startStories(from: Int) {
        // do something
        for (i in progressBars.indices) {
            progressBars[i].clear()
        }
        for (i in 0 until from) {
            if (progressBars.size > i) {
                progressBars[i].setMaxWithoutCallback()
            }
        }
        if (progressBars.size > from) {
            progressBars[from].startProgress()
        }
    }

    fun destroy() {
        for (p in progressBars) {
            p.clear()
        }
    }

    // declare function abadon
    fun abandon() {
        if (progressBars.size > current && current >= 0) {
            progressBars[current].setMinWithoutCallback()
        }
    }

    // declare function pause
    fun pause() {
        if (current < 0) return
        progressBars[current].pauseProgress()
    }

    // declare function resume
    fun resume() {
        if (current < 0 && progressBars.size > 0) {
            progressBars[0].startProgress()
            return
        }
        progressBars[current].resumeProgress()
    }

    // declare function getProgressWithIndex
    fun getProgressWithIndex(index: Int): PausableProgressBar {
        return progressBars[index]
    }

    // create companion object
    companion object {
        //define property PROGRESS_BAR_LAYOUT_PARAM with value LayoutParams
        private val PROGRESS_BAR_LAYOUT_PARAM = LayoutParams(0, LayoutParams.WRAP_CONTENT, 1F)
        // define property SPACE_LAYOUT_PARAM with value SPACE_LAYOUT_PARAM
        private val SPACE_LAYOUT_PARAM = LayoutParams(5, LayoutParams.WRAP_CONTENT)
    }

    // create interface StoriesListener
    interface StoriesListener {
        fun onNext()
        fun onPrev()
        fun onComplete()
    }
}